##### CSS1（1996年）：

选择器： 基本选择器，如类型选择器、类选择器、ID选择器。

body {
  color: black;
}

.example {
  font-size: 16px;
}

#uniqueElement {
  background-color: #f00;
}

###### 盒模型： margin、border、padding。

.floating-box {
  float: left;
}

.clear-float {
  clear: both;
}
显示： display、visibility。
css

.hidden-element {
  display: none;
}

.invisible-element {
  visibility: hidden;
}

###### 盒模型增强： min-height、min-width、max-height、max-width。

css

.constrained-box {
  min-width: 300px;
  max-height: 200px;
}
伪类： :hover、:active、:focus。
css

a:hover {
  text-decoration: underline;
}

input:focus {
  border: 2px solid #ff0000;

###### 选择器： 属性选择器 ([attr=value], [attr~=value]).

css

input[type="text"] {
  width: 200px;
}

a[href^="https"] {
  color: green;
}

##### CSS2.1（2011年）：

主要是对CSS2的澄清和纠正，没有引入大量新特性。

##### CSS3（工作草案，持续发展中）：

选择器 Level 3:

###### 通用兄弟选择器 (~): 选择兄弟元素。

css

h2 ~ p {
  font-style: italic;
}

###### 相邻兄弟选择器 (+): 选择相邻的兄弟元素。

css

h2 + p {
  font-weight: bold;
}
Color 模块 Level 3:
RGBA 和 HSLA 颜色： rgba(), hsla()。
css

.transparent-bg {
  background-color: rgba(255, 0, 0, 0.5);
}

.hsla-text {
  color: hsla(120, 100%, 50%, 0.8);
}

背景和边框模块 Level 3:
边框图像： border-image.

css

.border-image-example {
  border-image: url(border.png) 30 30 round;
}
多重背景： background-image.
css

.multiple-bg {
  background-image: url(bg1.jpg), url(bg2.jpg);
  background-size: cover, auto;
  background-position: top right, center;
}

###### 文本模块 Level 3:

文本阴影： text-shadow.

css

h1 {
  text-shadow: 2px 2px 4px #000;
}
文本溢出： text-overflow.
css

.ellipsis-text {
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
}

###### 多列布局模块:

多列布局： column-count, column-gap.

css

.multi-column {
  column-count: 3;
  column-gap: 20px;
}

###### 变换:

2D 变换： translate(), rotate(), scale().

css

.transform-example {
  transform: translate(50px, 20px) rotate(45deg) scale(1.5);
}

###### 3D 变换： rotateX(), rotateY(), rotateZ().

css

.rotate3d-example {
  transform: rotate3d(1, 1, 0, 45deg);
}

###### 动画:

关键帧动画： @keyframes.

css

@keyframes slideIn {
    from {
        opacity: 0;
        transform: translateX(-100%);
    }

    to {
        opacity: 1;
        transform: translateX(0);
    }
  }

  .animated-element {
    animation: slideIn 1s ease-in-out;
  }

###### Flexbox:

弹性盒布局模块： display: flex, flex-grow, flex-shrink, flex-basis.

css

.flex-container {
  display: flex;
  justify-content: space-between;
}

.flex-item {
  flex-grow: 1;
}

###### 网格布局:

CSS 网格布局模块： display: grid, grid-template-rows, grid-template-columns.

css

.grid-container {
  display: grid;
  grid-template-columns: 1fr 2fr 1fr;
}

.grid-item {
  grid-column: 2;
}

##### CSS4（工作草案，持续发展中）:

可变字体： 支持可变字体。

css

.variable-font-example {
  font-variation-settings: 'wght' 600, 'wdth' 100;
}

###### 容器查询： 根据容器大小调整样式。

css

@container (min-width: 600px) {
  .container-styles {
    font-size: 18px;
    color: #333;
  }
}
请注意，CSS的版本和特性不断发展，具体的特性和规范可能会有所调整。建议查阅 W3C（World Wide Web Consortium）的官方文档以获取最新的信息。以上示例仅供参考，并可能不涵盖所有特性和用法。
