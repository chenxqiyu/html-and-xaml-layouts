https://gitee.com/chenxqiyu/html-and-xaml-layouts.git

html学习
	盒子模型
		手机盒
			yuansu
		手机
			内容
		手机泡沫
			内间距
	元素
		块元素
			块元素在页面中以区域块的形式出现，其特点如下：• 独自占据一整行或多行；• 可对其设置宽度、高度、对齐等属性；• 可以容纳行内元素和其他块元素。常见的块元素有`<h1>`～`<h6>`、`<p>`、`<div>`、`<ul>`、`<ol>`、`<li>`等，其中`<div>`标记是最典型的块元素。
		行内元素
			行内元素也称内联元素或内嵌元素，其特点如下：• 和其他行内元素都在同一行上显示，不会独自换行；• 宽度就是它的文字或图片的宽度，默认不可改变；• 设置高度height无效，可以通过line-height来设置；• 设置margin只有左右margin有效，上下无效；• 设置padding只有左右padding有效，上下则无效；• 只能容纳文本或者其他行内元素。常见的行内元素有`<strong>`、`<b>`、`<em>`、`<i>`、`<del>`、`<s>`、`<ins>`、`<u>`、`<a>`、`<span>`等，其中`<span>`标记是最典型的行内元素
	元素的转换
		display
			inline
				行元素
			block
				块元素
	元素的浮动
		在CSS中，通过float属性来定义浮动，所谓元素的浮动是指设置了浮动属性的元素会脱离标准文档流的控制，移动到其父元素中指定位置的过程。
		• left：对象居左浮动，文本流向对象的右侧。• right：对象居右浮动，文本流向对象的左侧。• none：对象不浮动，该值为默认值。
	元素的定位
		元素的定位属性主要包括定位模式和边偏移两部分
	对比
		XAML
			块元素（Block Elements）
				Grid： 用于创建表格布局。
				StackPanel： 用于垂直或水平堆叠子元素。
				Border： 用于定义包围内容的边框。
				TextBlock： 用于显示文本。
			行内元素（Inline Elements）
				Run： 用于在TextBlock中定义文本的一部分。
				Span： 用于将文本或其他内联元素组合在一起。
		html
			块元素
				div： 通用块级容器，用于组织页面结构。
				p： 用于定义段落。
				h1, h2, h3, ...： 用于定义标题级别。
				ul, ol： 分别用于无序和有序列表。
				table： 用于创建表格。
			行内元素
				span： 通用行内容器，用于组织行内元素。
				a： 用于创建超链接。
				strong： 用于强调文本的重要性，通常表示加粗。
				em： 用于强调文本的斜体效果。
	Maui xaml
		StackLayout（堆栈布局）
			StackLayout 允许子元素按照水平或垂直方向堆叠在一起。
			`<StackLayout>`	`<Label Text="Item 1" />`	`<Label Text="Item 2" />`	`<Label Text="Item 3" />``</StackLayout>`
		GridLayout（网格布局
			GridLayout 允许你以网格的形式排列子元素
			`<GridLayout Rows="*,*,*" Columns="*,*">`	`<Label Text="1,1" Grid.Row="0" Grid.Column="0" />`	`<Label Text="1,2" Grid.Row="0" Grid.Column="1" />`	`<Label Text="2,1" Grid.Row="1" Grid.Column="0" />`	`<Label Text="2,2" Grid.Row="1" Grid.Column="1" />``</GridLayout>`
		FlexLayout（弹性布局）:
			FlexLayout 允许你使用弹性盒子模型，更灵活地排列子元素。
			`<FlexLayout Direction="Row" JustifyContent="SpaceBetween">`	`<Label Text="Item 1" />`	`<Label Text="Item 2" />`	`<Label Text="Item 3" />``</FlexLayout>`
		RelativeLayout（相对布局）:
			RelativeLayout 允许你按照相对位置放置子元素。
			`<RelativeLayout>`	`<Label Text="Top-left" RelativeLayout.XConstraint="{ConstraintExpression Type=RelativeToParent, Property=X, Factor=0}" RelativeLayout.YConstraint="{ConstraintExpression Type=RelativeToParent, Property=Y, Factor=0}" />`	`<Label Text="Bottom-right" RelativeLayout.XConstraint="{ConstraintExpression Type=RelativeToParent, Property=X, Factor=1}" RelativeLayout.YConstraint="{ConstraintExpression Type=RelativeToParent, Property=Y, Factor=1}" />``</RelativeLayout>`
		ScrollView（滚动视图）:
			ScrollView 允许你创建可滚动的视图，用于显示超出屏幕范围的内容。
			`<ScrollView>`	`<!-- 放置需要滚动的内容 -->`	`<StackLayout>`	    `<Label Text="Item 1" />`	    `<Label Text="Item 2" />`	    `<!-- 更多内容 -->`	`</StackLayout>``</ScrollView>`
